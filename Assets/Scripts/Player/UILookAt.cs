﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILookAt : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (Camera.main != null) {
			this.transform.LookAt (Camera.main.transform.position);
		}
	}
}
