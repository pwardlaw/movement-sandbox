﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]

public class Tuple<T1, T2>
{
	public T1 First { get; private set; }
	public T2 Second { get; private set; }
	internal Tuple(T1 first, T2 second)
	{
		First = first;
		Second = second;
	}
}

public class CharacterMovement : MonoBehaviour {

	//Modifiable settings
	//Standard
	public float speed = 3.0f; 
	public float runMultiplier = 1.5f;
	public float gravity = 10.0f;
	public float acceleration = 10.0f;
	public float decceleration = 15.0f;

	//Sprinting
	public float staminaDrainRate = 30f;
	public float staminaRegen = 40.0f;
	public float staminaTotal = 100.0f;

	//Dodging
	public float dodgeKeyInterval = 0.2f;

	//Jumping
	public float jumpForce = 1.0f;
	public int jumpLimit = 2;

	//Private Variables
	//Dodge mechanics
	private List<Tuple<int,float>> strafeKey;
	private const int LEFT = -1;
	private const int RIGHT = 1;
	private const int RELEASE = 0;

	//Jumping
	private bool jumpReleased = true;
	private int grounded = 0;

	//Running
	private float stamina = 0.0f;
	private bool ignoreSprint = false;


	/// <summary>
	/// Initializes a new instance of the <see cref="CharacterMovement"/> class.
	/// </summary>
	public CharacterMovement(){
		strafeKey = new List<Tuple<int,float>>();
	}

	public void FixedUpdate () {
		standardMovement ();
		doubleStrafe();
		jump ();
		applyGravity ();
	}

	public void Awake () {
		GetComponent<Rigidbody>().freezeRotation = true;
		GetComponent<Rigidbody>().useGravity = false;
	}

	public void OnCollisionStay () {
		grounded = 0;    
	}

	/// <summary>
	/// Perform Standard movement
	/// </summary>
	private void standardMovement ()
	{
		Vector3 inputVector = new Vector3(0, 0, 0);

		if(Input.GetKey(KeyCode.A))
			inputVector.x -=1;

		if(Input.GetKey(KeyCode.D))
			inputVector.x +=1;

		if(Input.GetKey(KeyCode.W))
			inputVector.z +=1;

		if(Input.GetKey(KeyCode.S))
			inputVector.z -=1;

		inputVector = inputVector.normalized;

		float acc;
		if (inputVector.magnitude == 0.0)
			acc = decceleration;
		else
			acc = acceleration;

		float sprint = 1.0f;

		if (Input.GetKey (KeyCode.LeftShift) && stamina >= 0.0f && !ignoreSprint) {
			sprint = runMultiplier;
			stamina -= staminaDrainRate * Time.deltaTime;
		
			if (stamina < 0.0f)
				ignoreSprint = true;
		
		}
		else if (stamina < staminaTotal) {
				stamina = Mathf.Min (staminaTotal, stamina + staminaRegen * Time.deltaTime);
		}

		if (!Input.GetKey (KeyCode.LeftShift))
			ignoreSprint = false;

		//Consider current velocity and target
		Vector3 velocity = GetComponent<Rigidbody> ().velocity;
		Vector3 targetVelocity = transform.TransformDirection (inputVector) * speed * sprint;

		//Determine transition force vector
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange.y = 0;

		//Correct for overshoot
		if ((velocityChange / Time.deltaTime).magnitude < Mathf.Abs (acc))
			acc = (velocityChange / Time.deltaTime).magnitude;

		//Calculate and Apply force
		float force = GetComponent<Rigidbody> ().mass * acc;
		if (force != 0)
			GetComponent<Rigidbody> ().AddForce (velocityChange.normalized * force, ForceMode.Force);
	}

	private void jump ()
	{
		if (Input.GetButton ("Jump") && (grounded < jumpLimit) && (jumpReleased == true)) {
			GetComponent<Rigidbody> ().AddForce (new Vector3 (0, jumpForce, 0), ForceMode.Impulse);
			jumpReleased = false;
			grounded += 1;
		} else if(!Input.GetButton ("Jump")) {
			jumpReleased = true;
		}
	}

	private void applyGravity ()
	{
		GetComponent<Rigidbody> ().AddForce (new Vector3 (0, -gravity * GetComponent<Rigidbody> ().mass, 0), ForceMode.Force);
	}

	private void doubleStrafe(){
		
		if (Input.GetKey (KeyCode.A)) {
			if((strafeKey.Count == 0) || strafeKey[strafeKey.Count -1].First == RELEASE)
				strafeKey.Add(new Tuple<int,float>(LEFT,Time.time));

		} else if (Input.GetKey (KeyCode.D) ) {
			if((strafeKey.Count == 0) || strafeKey[strafeKey.Count -1].First == RELEASE)
				strafeKey.Add(new Tuple<int,float>(RIGHT,Time.time));
		} else {
			if((strafeKey.Count != 0) && strafeKey[strafeKey.Count -1].First != RELEASE)
				strafeKey.Add(new Tuple<int,float>(RELEASE,0));
		}

		int dodgeBoostDirection = 0;

		if (strafeKey.Count == 3) {
			if((strafeKey[2].First == strafeKey[0].First) && (strafeKey[2].Second - strafeKey[0].Second) < dodgeKeyInterval){
				dodgeBoostDirection = strafeKey[0].First;
			}
			strafeKey.Clear();
		}

		if (dodgeBoostDirection != 0) {
			Vector3 dodge = new Vector3 (dodgeBoostDirection * 10, 0, 0);
			transform.TransformDirection (dodge);
			GetComponent<Rigidbody> ().AddForce (dodge, ForceMode.Impulse);
		}
	
	}
}
